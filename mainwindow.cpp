#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qdebug.h>
#include <qinputdialog.h>
#include <qdir.h>
#include <sstream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), ui(new Ui::MainWindow)
{
    // Pay attention to the information you enter here.
 //   appSettings = new QSettings("Contingency Bloggers Corporation",
                                //"Save State", this);
 //   qDebug() << appSettings->fileName();
    // Restore the UI


    ui->setupUi(this);
 //   readSettings();
}

MainWindow::~MainWindow()
{
    delete ui;
}


 // Inject code into the Close Event
void MainWindow::closeEvent(QCloseEvent *)
{
    // This will be called whenever this window is closed.
   // writeSettings();
}


void MainWindow::writeSettings()
{
    // Pay attention to the information you enter here.
    appSettings = new QSettings("Contingency Bloggers Corporation",
                                getFileName(), this);
    qDebug() << appSettings->fileName();
    // Write the values to disk in categories.
    appSettings->setValue("state/mainWindowState", saveState());
    appSettings->setValue("geometry/mainWindowGeometry",
                                           saveGeometry());
    appSettings->setValue("radioBtn", ui->radioButton->isChecked());
    appSettings->setValue("text", ui->textEdit->toPlainText());
    storeTable();
}
void MainWindow::storeTable()
{
 //   std::string index;
 //   if (ui->tableWidget->rowCount() != 0)
//    {
        appSettings->beginWriteArray("table");
        for (int j = 0; j < ui->tableWidget->rowCount(); j++)
        {
            for (int i = 0; i < 2; ++i) {
                 appSettings->setArrayIndex(j*ui->tableWidget->rowCount()+i);
                 appSettings->setValue("col", ui->tableWidget->item(j,i)->text());
             }
        }

        appSettings->endArray();

//    }

}

QString MainWindow::getFileName()
{
    bool ok;
    QString text = QInputDialog::getText(this, tr("please input the file name"), tr("file name"), QLineEdit::Normal, QDir::home().dirName(), &ok);
    return text;
}

void MainWindow::restore()
{
    appSettings = new QSettings("Contingency Bloggers Corporation",
                                getFileName(), this);
    // We have to call toByteArray because the
    // values are stored as QVariant's
    QByteArray stateData = appSettings->value
            ("state/mainWindowState").toByteArray();

    QByteArray geometryData = appSettings->value
            ("geometry/mainWindowGeometry").toByteArray();
    QByteArray radioData = appSettings->value("radioBtn").toByteArray();

    restoreState(stateData);
    restoreGeometry(geometryData);
    restoreRadio();
    restoreText();
    restoreTable();
}

void MainWindow::restoreRadio()
{
    ui->radioButton->setChecked(appSettings->value("radioBtn").toBool());
}

void MainWindow::restoreText()
{
    ui->textEdit->setText(appSettings->value("text").toString());
}

void MainWindow::restoreTable()
{
    // remove all rows
    while (ui->tableWidget->rowCount() > 0)
    {
        ui->tableWidget->removeRow(0);
    }
    int size = appSettings->beginReadArray("table");
    qDebug() << "size: " << size;
    for (int j = 0; j < size / ui->tableWidget->columnCount(); j++)
    {
        ui->tableWidget->insertRow(j);
        for (int i = 0; i < ui->tableWidget->columnCount(); ++i)
        {
            appSettings->setArrayIndex(j*ui->tableWidget->columnCount() + i);
            QTableWidgetItem *cell_in_row = new QTableWidgetItem(appSettings->value("col").toString());
            ui->tableWidget->setItem(j, i, cell_in_row);
        }
    }
}

void MainWindow::readSettings()
{
    // This is a call to our restore() method.
    // We use this so we can keep the purposes of the
    // different methods clear if we were to expand.
    restore();
}

void MainWindow::on_actionQuit_triggered()
{
    // Make sure we write the settings. We could close the window,
    // but I prefer using qApp->quit() to clean up anything else.
//    writeSettings();

    qApp->quit();
}

void MainWindow::on_radioButton_clicked()
{
    qDebug() << "clicked: " << ui->radioButton->isChecked();
}

void MainWindow::on_pushButton_clicked()
{
    writeSettings();
}

void MainWindow::on_pushButton_2_clicked()
{
    readSettings();
}

void MainWindow::on_pushButton_3_clicked()
{
    int row = ui->tableWidget->rowCount();
    qDebug() << "row count: " << row;
    ui->tableWidget->insertRow(row);
}

void MainWindow::on_pushButton_4_clicked()
{
    ui->tableWidget->removeRow(ui->tableWidget->currentRow());
}
