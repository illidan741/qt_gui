#include "mainwindow.h"
#include <QApplication>

// For Fusion Style
#include <QStyleFactory>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    // Set the application's style to Fusion
    QApplication::setStyle(QStyleFactory::create("Fusion"));

    // Use your real info here. This is the information QSettings
    // will use to store our settings later.
    QApplication::setOrganizationDomain("com.wordpress.contigencycoder");
    QApplication::setOrganizationName("Contingency Bloggers Corporation");
    QApplication::setApplicationName("Save State");
    QApplication::setApplicationVersion("1.0.0");

    // Create MainWindow and show it...
    MainWindow window;
    window.show();

    // Start out app's event loop
    return app.exec();
}
