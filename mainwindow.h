#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSettings>
#include <QByteArray>

namespace Ui
{
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void closeEvent(QCloseEvent *);

private slots:
    void on_actionQuit_triggered();

    void on_radioButton_clicked();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

private:
    Ui::MainWindow *ui;
    QSettings *appSettings;

    // Some functions we will use for restoring/writing the
    // necessary data
    void writeSettings();
    void readSettings();
    void restore();
    void restoreRadio();
    void restoreText();
    QString getFileName();
    void restoreTable();
    void storeTable();
};

#endif // MAINWINDOW_H
